import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { styles } from './style';

const Teclado = ({onClick}) => {
   
    return (
        <View style={styles.Conteiner}>
            <View style={styles.linha}>
                    <View style={styles.containerButton}>
                        <TouchableOpacity 
                        onPress={()=>{
                        onClick('clc')
                       
                        }}>
                            <View style={styles.botao}>
                                <Text style={styles.text}> CLEAR</Text>
                            </View>
                                
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerButton}>
                        <TouchableOpacity 
                        onPress={()=>{
                            onClick('del')}}>
                            <View style={styles.botao}>
                                <Text style={styles.text}>⊲</Text>
                            </View>
                                
                        </TouchableOpacity>
                    </View>
            </View>
        
            <View style={styles.linha}>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('1')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>1</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('2')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>2</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('3')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>3</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('÷')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>÷</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
               
                
            </View>
            <View style={styles.linha}>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('4')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>4</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('5')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>5</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('6')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>6</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('X')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>X</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
               
                
            </View>
            <View style={styles.linha}>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('7')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>7</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('8')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>8</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('9')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>9</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('-')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>-</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
               
                
            </View>
            <View style={styles.linha}>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('.')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>.</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('0')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>0</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('=')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>=</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
                <View style={styles.containerButton}>
                    <TouchableOpacity 
                    onPress={()=>{onClick('+')}}>
                        <View style={styles.botao}>
                            <Text style={styles.text}>+</Text>
                        </View>
                            
                    </TouchableOpacity>
                </View>
               
                
            </View>
        </View>
    )
}
export default Teclado;


