import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    Conteiner: {
        flex: 1,
        
        backgroundColor: '#2150ff',
        
    },
    linha: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around'
        
    },
    botao: {
        
        backgroundColor: '#2196f3',
        height: '100%',
        alignContent: 'center',
        justifyContent: 'center'
        
    },
    containerButton: {
        flex: 1,
        padding: 1,
        margin: 1
        
    },
    text: {
        color: 'white',
        fontSize: 26,
        textAlign: 'center',
        textAlignVertical: 'center'

    }
  });