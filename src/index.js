
import React, {useState, Component} from 'react';
import {
  Text,
  View,
  StatusBar,
} from 'react-native';

import { styles } from './style';
import Teclado from './Teclado/index';



export default class App extends Component{
  state = {value: '0'};
  componentDidMount(){
    this.setState({value: '0'});
    this.lastCharIsOperator = false;
    this.calculada = false;
  }
  isNumber = (num) => !isNaN(parseFloat(num)) && isFinite(num);
  Click = (text) => {
   //alert(this.lastCharIsOperator);
   let  value  = this.state.value;
   let aux = (value === '0' && !(['X','-','+'].includes(text)))? '': value;
   //aux = (!aux)? '': aux;
   //alert(aux)
   if (aux === 'Erro!') {
     aux = '';
   } 
   switch (text) {
     case 'del':
         const index = aux.length-1;
         const t = aux[index];
         if(['X','-','÷','+'].includes(t)) {
           this.lastCharIsOperator = false;
         }
         aux = aux.slice(0,index);
       break;
     case 'clc':
         this.lastCharIsOperator = false;
         aux = '';
       break;
     case '=':
         this.lastCharIsOperator = false;
         
         aux = aux.replace('X','*').replace('÷','/');
         try{
           if(!this.isNumber(aux)) {
             this.calculada = true;
             
             aux = eval(aux);
             
             aux = ''+Math.floor(aux.toFixed(2));
             aux = (aux !== 'undefined')? aux : ''
           } 
         } catch(e) {
           aux = 'Erro!';
         }
         
       break;
   
     default:
         
         if(['X','-','÷','+'].includes(text)) {
           //alert(this.lastCharIsOperator)
           this.calculada = false;
           if(this.lastCharIsOperator) {
             //alert('entrou')
             const index = aux.length-1;
             aux = aux.slice(0,index);
           } else {
             this.lastCharIsOperator = true;
             //alert(this.lastCharIsOperator)
           }
         } else if (this.calculada && this.isNumber(text)) {
           this.calculada = false;
           aux = '';
         }
        
         aux += text;
       break;
   }
 
   aux = (aux === '')? '0': aux;
   aux = (aux === 'Infinity')? '∞': aux;
   this.setState({value: aux})
 
 }
  
 render() {
  return (
    <>
    <StatusBar barStyle="light-content" backgroundColor="#35f"/>
    <View style={styles.Conteiner}>
        <View style={styles.result}>
          <Text style={styles.resultText}>{this.state.value}</Text>
        </View>
        <View style={styles.inputText}>
          <Teclado onClick={this.Click}/>
        </View>
        
    </View>
    </>
  
  );
}

};




